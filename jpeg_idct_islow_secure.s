	.file	"jpeg_idct_islow_secure.s"
	.section	.rodata
.LC0:
	.string	"cmov.h"
.LC1:
	.string	"pred == 0 || pred == 1"
	.text
	.type	cmov, @function
cmov:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movl	$666, -4(%rbp)
	cmpl	$0, -20(%rbp)
	je	.L2
	cmpl	$1, -20(%rbp)
	je	.L2
	movl	$__PRETTY_FUNCTION__.4216, %ecx
	movl	$11, %edx
	movl	$.LC0, %esi
	movl	$.LC1, %edi
	call	__assert_fail
.L2:
	movl	-20(%rbp), %edx
	movl	-24(%rbp), %ecx
	movl	-28(%rbp), %esi
#APP
# 26 "cmov.h" 1
	cmpl $0, %edx
	cmovne %ecx, %eax
	cmove %esi, %eax
	movl %eax, %edx

# 0 "" 2
#NO_APP
	movl	%edx, -4(%rbp)
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	cmov, .-cmov
	.section	.rodata
.LC2:
	.string	"1."
	.text
	.globl	jpeg_idct_islow
	.type	jpeg_idct_islow, @function
jpeg_idct_islow:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$1032, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -1000(%rbp)
	movq	%rsi, -1008(%rbp)
	movq	%rdx, -1016(%rbp)
	movq	%rcx, -1024(%rbp)
	movl	%r8d, -1028(%rbp)
	movq	-1000(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -72(%rbp)
	movq	-1016(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-1008(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -32(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -40(%rbp)
	leaq	-704(%rbp), %rax
	movq	%rax, -56(%rbp)
	leaq	-960(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	$8, -44(%rbp)
	jmp	.L5
.L10:
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movl	$1, %eax
	jmp	.L7
.L6:
	movl	$0, %eax
.L7:
	movl	%eax, -76(%rbp)
#APP
# 114 "jpeg_idct_islow_secure.c" 1
# 0 "" 2
#NO_APP
	cmpl	$0, -76(%rbp)
	.byte 0x2e
	je	.L8
	movq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	sall	$2, %eax
	movl	%eax, -80(%rbp)
	movq	-56(%rbp), %rax
	movl	-80(%rbp), %edx
	movl	%edx, (%rax)
	movq	-56(%rbp), %rax
	leaq	32(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	64(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	96(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	128(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	160(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	192(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	224(%rax), %rdx
	movl	-80(%rbp), %eax
	movl	%eax, (%rdx)
	jmp	.L9
.L8:
	movq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	salq	$13, -88(%rbp)
	salq	$13, -96(%rbp)
	addq	$1024, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-112(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-24(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-104(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-64(%rbp), %rax
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-64(%rbp), %rax
	addq	$32, %rax
	movq	-160(%rbp), %rcx
	movq	-128(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-64(%rbp), %rax
	addq	$64, %rax
	movq	-168(%rbp), %rcx
	movq	-112(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-64(%rbp), %rax
	addq	$96, %rax
	movq	-152(%rbp), %rcx
	movq	-104(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	128(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
.L9:
#APP
# 231 "jpeg_idct_islow_secure.c" 1
	.byte 0x2e,0x90
# 0 "" 2
#NO_APP
	movq	-64(%rbp), %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, %edx
	movq	-40(%rbp), %rax
	movl	%edx, (%rax)
	movq	-40(%rbp), %rax
	leaq	32(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	64(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	96(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	128(%rax), %rbx
	movq	-64(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	160(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	192(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	224(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %ecx
	movl	-76(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	addq	$4, -64(%rbp)
	addq	$4, -56(%rbp)
	addq	$2, -24(%rbp)
	addq	$4, -32(%rbp)
	addq	$4, -40(%rbp)
	subl	$1, -44(%rbp)
.L5:
	cmpl	$0, -44(%rbp)
	jg	.L10
	leaq	-448(%rbp), %rax
	movq	%rax, -40(%rbp)
	movl	$0, -44(%rbp)
	jmp	.L11
.L16:
	movl	-44(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-1024(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-1028(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-40(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -88(%rbp)
	movl	$0, -180(%rbp)
	movq	-40(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movl	$1, %eax
	jmp	.L13
.L12:
	movl	$0, %eax
.L13:
	movl	%eax, -180(%rbp)
#APP
# 314 "jpeg_idct_islow_secure.c" 1
# 0 "" 2
#NO_APP
	cmpl	$0, -180(%rbp)
	.byte 0x2e
	je	.L14
	movq	-88(%rbp), %rax
	sarq	$5, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -181(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -976(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -975(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -974(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -973(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -972(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -971(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -970(%rbp)
	movzbl	-181(%rbp), %eax
	movb	%al, -969(%rbp)
	jmp	.L15
.L14:
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -112(%rbp)
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-40(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-112(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-40(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-40(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-104(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -992(%rbp)
	movq	-144(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -985(%rbp)
	movq	-160(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -991(%rbp)
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -986(%rbp)
	movq	-168(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -990(%rbp)
	movq	-168(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -987(%rbp)
	movq	-152(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -989(%rbp)
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -988(%rbp)
.L15:
#APP
# 465 "jpeg_idct_islow_secure.c" 1
	.byte 0x2e,0x90
# 0 "" 2
#NO_APP
	movzbl	-992(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-976(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, %edx
	movq	-176(%rbp), %rax
	movb	%dl, (%rax)
	movq	-176(%rbp), %rax
	leaq	1(%rax), %rbx
	movzbl	-991(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-975(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-176(%rbp), %rax
	leaq	2(%rax), %rbx
	movzbl	-990(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-974(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-176(%rbp), %rax
	leaq	3(%rax), %rbx
	movzbl	-989(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-973(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-176(%rbp), %rax
	leaq	4(%rax), %rbx
	movzbl	-988(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-972(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-176(%rbp), %rax
	leaq	5(%rax), %rbx
	movzbl	-987(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-971(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-176(%rbp), %rax
	leaq	6(%rax), %rbx
	movzbl	-986(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-970(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-176(%rbp), %rax
	leaq	7(%rax), %rbx
	movzbl	-985(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-969(%rbp), %eax
	movzbl	%al, %ecx
	movl	-180(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	addq	$32, -40(%rbp)
	addl	$1, -44(%rbp)
.L11:
	cmpl	$7, -44(%rbp)
	jle	.L16
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movq	stdout(%rip), %rax
	movq	%rax, %rdi
	call	fflush
	addq	$1032, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	jpeg_idct_islow, .-jpeg_idct_islow
	.section	.rodata
	.type	__PRETTY_FUNCTION__.4216, @object
	.size	__PRETTY_FUNCTION__.4216, 5
__PRETTY_FUNCTION__.4216:
	.string	"cmov"
	.ident	"GCC: (Debian 4.9.2-10+deb8u2) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
