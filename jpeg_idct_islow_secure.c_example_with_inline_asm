#define JPEG_INTERNALS
#include "jinclude.h"
#include "jpeglib.h"
#include "jdct.h"		/* Private declarations for DCT subsystem */

#include <stdbool.h>
#include <assert.h>
#include "cmov.h"

#ifdef DCT_ISLOW_SUPPORTED

#if DCTSIZE != 8
  Sorry, this code only copes with 8x8 DCT blocks. /* deliberate syntax err */
#endif

#if BITS_IN_JSAMPLE == 8
#define CONST_BITS  13
#define PASS1_BITS  2
#else
#define CONST_BITS  13
#define PASS1_BITS  1		/* lose a little precision to avoid overflow */
#endif


#if CONST_BITS == 13
#define FIX_0_298631336  ((INT32)  2446)	/* FIX(0.298631336) */
#define FIX_0_390180644  ((INT32)  3196)	/* FIX(0.390180644) */
#define FIX_0_541196100  ((INT32)  4433)	/* FIX(0.541196100) */
#define FIX_0_765366865  ((INT32)  6270)	/* FIX(0.765366865) */
#define FIX_0_899976223  ((INT32)  7373)	/* FIX(0.899976223) */
#define FIX_1_175875602  ((INT32)  9633)	/* FIX(1.175875602) */
#define FIX_1_501321110  ((INT32)  12299)	/* FIX(1.501321110) */
#define FIX_1_847759065  ((INT32)  15137)	/* FIX(1.847759065) */
#define FIX_1_961570560  ((INT32)  16069)	/* FIX(1.961570560) */
#define FIX_2_053119869  ((INT32)  16819)	/* FIX(2.053119869) */
#define FIX_2_562915447  ((INT32)  20995)	/* FIX(2.562915447) */
#define FIX_3_072711026  ((INT32)  25172)	/* FIX(3.072711026) */
#else
#define FIX_0_298631336  FIX(0.298631336)
#define FIX_0_390180644  FIX(0.390180644)
#define FIX_0_541196100  FIX(0.541196100)
#define FIX_0_765366865  FIX(0.765366865)
#define FIX_0_899976223  FIX(0.899976223)
#define FIX_1_175875602  FIX(1.175875602)
#define FIX_1_501321110  FIX(1.501321110)
#define FIX_1_847759065  FIX(1.847759065)
#define FIX_1_961570560  FIX(1.961570560)
#define FIX_2_053119869  FIX(2.053119869)
#define FIX_2_562915447  FIX(2.562915447)
#define FIX_3_072711026  FIX(3.072711026)
#endif

#if BITS_IN_JSAMPLE == 8
#define MULTIPLY(var,const)  MULTIPLY16C16(var,const)
#else
#define MULTIPLY(var,const)  ((var) * (const))
#endif

#define DEQUANTIZE(coef,quantval)  (((ISLOW_MULT_TYPE) (coef)) * (quantval))

GLOBAL(void)
jpeg_idct_islow (j_decompress_ptr cinfo, jpeg_component_info * compptr,
		 JCOEFPTR coef_block,
		 JSAMPARRAY output_buf, JDIMENSION output_col)
{
  // printf("Running SECURE block of %s\n",__func__);
  // assert(0);
  INT32 tmp0, tmp1, tmp2, tmp3;
  INT32 tmp10, tmp11, tmp12, tmp13;
  INT32 z1, z2, z3;
  JCOEFPTR inptr;
  ISLOW_MULT_TYPE * quantptr;
  int * wsptr;
  JSAMPROW outptr;
  JSAMPLE *range_limit = IDCT_range_limit(cinfo);
  int ctr;
  int workspace[DCTSIZE2];	/* buffers data between passes */
  SHIFT_TEMPS

  /* Pass 1: process columns from input, store into work array.
   * Note results are scaled up by sqrt(8) compared to a true IDCT;
   * furthermore, we scale the results by 2**PASS1_BITS.
   */

  inptr = coef_block;
  quantptr = (ISLOW_MULT_TYPE *) compptr->dct_table;
  wsptr = workspace;

  int workspace_true[DCTSIZE2];
  int workspace_false[DCTSIZE2];
  int * wsptr_true;
  int * wsptr_false;
  wsptr_true = workspace_true;
  wsptr_false = workspace_false;

  for (ctr = DCTSIZE; ctr > 0; ctr--) {
    /* Due to quantization, we will usually find that many of the input
     * coefficients are zero, especially the AC terms.  We can exploit this
     * by short-circuiting the IDCT calculation for any column in which all
     * the AC terms are zero.  In that case each output is equal to the
     * DC coefficient (with scale factor as needed).
     * With typical images and quantization tables, half or more of the
     * column DCT calculations can be simplified this way.
     */

    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    if (inptr[DCTSIZE*1] == 0
    &&
    inptr[DCTSIZE*2] == 0
    &&
    inptr[DCTSIZE*3] == 0
    &&
    inptr[DCTSIZE*4] == 0
    &&
    inptr[DCTSIZE*5] == 0
    &&
    inptr[DCTSIZE*6] == 0
    &&
    inptr[DCTSIZE*7] == 0)
    {
        /* AC terms all zero */
        int dcval = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]) << PASS1_BITS;

        // wsptr[DCTSIZE*0] = dcval;
        // wsptr[DCTSIZE*1] = dcval;
        // wsptr[DCTSIZE*2] = dcval;
        // wsptr[DCTSIZE*3] = dcval;
        // wsptr[DCTSIZE*4] = dcval;
        // wsptr[DCTSIZE*5] = dcval;
        // wsptr[DCTSIZE*6] = dcval;
        // wsptr[DCTSIZE*7] = dcval;
        wsptr_true[DCTSIZE*0] = dcval;
        wsptr_true[DCTSIZE*1] = dcval;
        wsptr_true[DCTSIZE*2] = dcval;
        wsptr_true[DCTSIZE*3] = dcval;
        wsptr_true[DCTSIZE*4] = dcval;
        wsptr_true[DCTSIZE*5] = dcval;
        wsptr_true[DCTSIZE*6] = dcval;
        wsptr_true[DCTSIZE*7] = dcval;

        // inptr++;			/* advance pointers to next column */
        // quantptr++;
        // wsptr++;
    }
    else
    {
      /* Even part: reverse the even part of the forward DCT.
      * The rotator is c(-6).
      */

      z2 = DEQUANTIZE(inptr[DCTSIZE*0], quantptr[DCTSIZE*0]);
      z3 = DEQUANTIZE(inptr[DCTSIZE*4], quantptr[DCTSIZE*4]);
      z2 <<= CONST_BITS;
      z3 <<= CONST_BITS;
      /* Add fudge factor here for final descale. */
      z2 += ONE << (CONST_BITS-PASS1_BITS-1);

      tmp0 = z2 + z3;
      tmp1 = z2 - z3;

      z2 = DEQUANTIZE(inptr[DCTSIZE*2], quantptr[DCTSIZE*2]);
      z3 = DEQUANTIZE(inptr[DCTSIZE*6], quantptr[DCTSIZE*6]);

      z1 = MULTIPLY(z2 + z3, FIX_0_541196100);       /* c6 */
      tmp2 = z1 + MULTIPLY(z2, FIX_0_765366865);     /* c2-c6 */
      tmp3 = z1 - MULTIPLY(z3, FIX_1_847759065);     /* c2+c6 */

      tmp10 = tmp0 + tmp2;
      tmp13 = tmp0 - tmp2;
      tmp11 = tmp1 + tmp3;
      tmp12 = tmp1 - tmp3;

      /* Odd part per figure 8; the matrix is unitary and hence its
      * transpose is its inverse.  i0..i3 are y7,y5,y3,y1 respectively.
      */

      tmp0 = DEQUANTIZE(inptr[DCTSIZE*7], quantptr[DCTSIZE*7]);
      tmp1 = DEQUANTIZE(inptr[DCTSIZE*5], quantptr[DCTSIZE*5]);
      tmp2 = DEQUANTIZE(inptr[DCTSIZE*3], quantptr[DCTSIZE*3]);
      tmp3 = DEQUANTIZE(inptr[DCTSIZE*1], quantptr[DCTSIZE*1]);

      z2 = tmp0 + tmp2;
      z3 = tmp1 + tmp3;

      z1 = MULTIPLY(z2 + z3, FIX_1_175875602);       /*  c3 */
      z2 = MULTIPLY(z2, - FIX_1_961570560);          /* -c3-c5 */
      z3 = MULTIPLY(z3, - FIX_0_390180644);          /* -c3+c5 */
      z2 += z1;
      z3 += z1;

      z1 = MULTIPLY(tmp0 + tmp3, - FIX_0_899976223); /* -c3+c7 */
      tmp0 = MULTIPLY(tmp0, FIX_0_298631336);        /* -c1+c3+c5-c7 */
      tmp3 = MULTIPLY(tmp3, FIX_1_501321110);        /*  c1+c3-c5-c7 */
      tmp0 += z1 + z2;
      tmp3 += z1 + z3;

      z1 = MULTIPLY(tmp1 + tmp2, - FIX_2_562915447); /* -c1-c3 */
      tmp1 = MULTIPLY(tmp1, FIX_2_053119869);        /*  c1+c3-c5+c7 */
      tmp2 = MULTIPLY(tmp2, FIX_3_072711026);        /*  c1+c3+c5-c7 */
      tmp1 += z1 + z3;
      tmp2 += z1 + z2;

      /* Final output stage: inputs are tmp10..tmp13, tmp0..tmp3 */

      // wsptr[DCTSIZE*0] = (int) RIGHT_SHIFT(tmp10 + tmp3, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*7] = (int) RIGHT_SHIFT(tmp10 - tmp3, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*1] = (int) RIGHT_SHIFT(tmp11 + tmp2, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*6] = (int) RIGHT_SHIFT(tmp11 - tmp2, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*2] = (int) RIGHT_SHIFT(tmp12 + tmp1, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*5] = (int) RIGHT_SHIFT(tmp12 - tmp1, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*3] = (int) RIGHT_SHIFT(tmp13 + tmp0, CONST_BITS-PASS1_BITS);
      // wsptr[DCTSIZE*4] = (int) RIGHT_SHIFT(tmp13 - tmp0, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*0] = (int) RIGHT_SHIFT(tmp10 + tmp3, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*7] = (int) RIGHT_SHIFT(tmp10 - tmp3, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*1] = (int) RIGHT_SHIFT(tmp11 + tmp2, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*6] = (int) RIGHT_SHIFT(tmp11 - tmp2, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*2] = (int) RIGHT_SHIFT(tmp12 + tmp1, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*5] = (int) RIGHT_SHIFT(tmp12 - tmp1, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*3] = (int) RIGHT_SHIFT(tmp13 + tmp0, CONST_BITS-PASS1_BITS);
      wsptr_false[DCTSIZE*4] = (int) RIGHT_SHIFT(tmp13 - tmp0, CONST_BITS-PASS1_BITS);

      // inptr++;			/* advance pointers to next column */
      // quantptr++;
      // wsptr++;
    }

    __asm__ volatile(".byte 0x2e,0x90":::);
    __asm__ volatile(".byte 0x2e,0x90":::);
    __asm__ volatile(".byte 0x2e,0x90":::);
    __asm__ volatile(".byte 0x2e,0x90":::);
    __asm__ volatile(".byte 0x2e,0x90":::);
    __asm__ volatile(".byte 0x2e,0x90":::);
    __asm__ volatile(".byte 0x2e,0x90":::);

    int condition = inptr[DCTSIZE * 1] == 0 &&
                     inptr[DCTSIZE * 2] == 0 &&
                     inptr[DCTSIZE * 3] == 0 &&
                     inptr[DCTSIZE * 4] == 0 &&
                     inptr[DCTSIZE * 5] == 0 &&
                     inptr[DCTSIZE * 6] == 0 &&
                     inptr[DCTSIZE * 7] == 0;

    wsptr[DCTSIZE*0] = cmov(condition , wsptr_true[DCTSIZE*0], wsptr_false[DCTSIZE*0]);
    wsptr[DCTSIZE*1] = cmov(condition , wsptr_true[DCTSIZE*1], wsptr_false[DCTSIZE*1]);
    wsptr[DCTSIZE*2] = cmov(condition , wsptr_true[DCTSIZE*2], wsptr_false[DCTSIZE*2]);
    wsptr[DCTSIZE*3] = cmov(condition , wsptr_true[DCTSIZE*3], wsptr_false[DCTSIZE*3]);
    wsptr[DCTSIZE*4] = cmov(condition , wsptr_true[DCTSIZE*4], wsptr_false[DCTSIZE*4]);
    wsptr[DCTSIZE*5] = cmov(condition , wsptr_true[DCTSIZE*5], wsptr_false[DCTSIZE*5]);
    wsptr[DCTSIZE*6] = cmov(condition , wsptr_true[DCTSIZE*6], wsptr_false[DCTSIZE*6]);
    wsptr[DCTSIZE*7] = cmov(condition , wsptr_true[DCTSIZE*7], wsptr_false[DCTSIZE*7]);

    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*0]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*0]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*1]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*1]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*2]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*2]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*3]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*3]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*4]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*4]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*5]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*5]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*6]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*6]);
    // printf("[debug] Iteration %d (%s), wsptr_true[DCTSIZE*7]=%d\n",ctr,condition ? "true" : "false" ,wsptr_true[DCTSIZE*7]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*0]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*0]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*1]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*1]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*2]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*2]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*3]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*3]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*4]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*4]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*5]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*5]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*6]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*6]);
    // printf("[debug] Iteration %d (%s), wsptr_false[DCTSIZE*7]=%d\n",ctr,condition ? "true" : "false" ,wsptr_false[DCTSIZE*7]);

    // printf("Iteration %d (%s), wsptr[DCTSIZE*0]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*0]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*1]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*1]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*2]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*2]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*3]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*3]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*4]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*4]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*5]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*5]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*6]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*6]);
    // printf("Iteration %d (%s), wsptr[DCTSIZE*7]=%d\n",ctr,condition ? "true" : "false" ,wsptr[DCTSIZE*7]);

    wsptr_false++;
    wsptr_true++;
    inptr++;			/* advance pointers to next column */
    quantptr++;
    wsptr++;

  }


  /* Pass 2: process rows from work array, store into output array.
   * Note that we must descale the results by a factor of 8 == 2**3,
   * and also undo the PASS1_BITS scaling.
   */

  wsptr = workspace;
  for (ctr = 0; ctr < DCTSIZE; ctr++) {
    outptr = output_buf[ctr] + output_col;

    JSAMPLE outptr_true[8];
    JSAMPLE outptr_false[8];

    /* Add range center and fudge factor for final descale and range-limit. */
    z2 = (INT32) wsptr[0] +
	   ((((INT32) RANGE_CENTER) << (PASS1_BITS+3)) +
	    (ONE << (PASS1_BITS+2)));

    /* Rows of zeroes can be exploited in the same way as we did with columns.
     * However, the column calculation has created many nonzero AC terms, so
     * the simplification applies less often (typically 5% to 10% of the time).
     * On machines with very fast multiplication, it's possible that the
     * test takes more time than it's worth.  In that case this section
     * may be commented out.
     */

#ifndef NO_ZERO_ROW_TEST

    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    __asm__ volatile(".byte 0x2e":::);
    if (
        wsptr[1] == 0
        &&
        wsptr[2] == 0
        &&
        wsptr[3] == 0
        &&
        wsptr[4] == 0
        &&
        wsptr[5] == 0
        &&
        wsptr[6] == 0
        &&
        wsptr[7] == 0
      ) {
      /* AC terms all zero */
      JSAMPLE dcval = range_limit[(int) RIGHT_SHIFT(z2, PASS1_BITS+3)
				  & RANGE_MASK];

      // outptr[0] = dcval;
      // outptr[1] = dcval;
      // outptr[2] = dcval;
      // outptr[3] = dcval;
      // outptr[4] = dcval;
      // outptr[5] = dcval;
      // outptr[6] = dcval;
      // outptr[7] = dcval;
      outptr_true[0] = dcval;
      outptr_true[1] = dcval;
      outptr_true[2] = dcval;
      outptr_true[3] = dcval;
      outptr_true[4] = dcval;
      outptr_true[5] = dcval;
      outptr_true[6] = dcval;
      outptr_true[7] = dcval;

      // wsptr += DCTSIZE;		/* advance pointer to next row */
      // continue;

    }
    else
    {
#endif

    /* Even part: reverse the even part of the forward DCT.
     * The rotator is c(-6).
     */

    z3 = (INT32) wsptr[4];

    tmp0 = (z2 + z3) << CONST_BITS;
    tmp1 = (z2 - z3) << CONST_BITS;

    z2 = (INT32) wsptr[2];
    z3 = (INT32) wsptr[6];

    z1 = MULTIPLY(z2 + z3, FIX_0_541196100);       /* c6 */
    tmp2 = z1 + MULTIPLY(z2, FIX_0_765366865);     /* c2-c6 */
    tmp3 = z1 - MULTIPLY(z3, FIX_1_847759065);     /* c2+c6 */

    tmp10 = tmp0 + tmp2;
    tmp13 = tmp0 - tmp2;
    tmp11 = tmp1 + tmp3;
    tmp12 = tmp1 - tmp3;

    /* Odd part per figure 8; the matrix is unitary and hence its
     * transpose is its inverse.  i0..i3 are y7,y5,y3,y1 respectively.
     */

    tmp0 = (INT32) wsptr[7];
    tmp1 = (INT32) wsptr[5];
    tmp2 = (INT32) wsptr[3];
    tmp3 = (INT32) wsptr[1];

    z2 = tmp0 + tmp2;
    z3 = tmp1 + tmp3;

    z1 = MULTIPLY(z2 + z3, FIX_1_175875602);       /*  c3 */
    z2 = MULTIPLY(z2, - FIX_1_961570560);          /* -c3-c5 */
    z3 = MULTIPLY(z3, - FIX_0_390180644);          /* -c3+c5 */
    z2 += z1;
    z3 += z1;

    z1 = MULTIPLY(tmp0 + tmp3, - FIX_0_899976223); /* -c3+c7 */
    tmp0 = MULTIPLY(tmp0, FIX_0_298631336);        /* -c1+c3+c5-c7 */
    tmp3 = MULTIPLY(tmp3, FIX_1_501321110);        /*  c1+c3-c5-c7 */
    tmp0 += z1 + z2;
    tmp3 += z1 + z3;

    z1 = MULTIPLY(tmp1 + tmp2, - FIX_2_562915447); /* -c1-c3 */
    tmp1 = MULTIPLY(tmp1, FIX_2_053119869);        /*  c1+c3-c5+c7 */
    tmp2 = MULTIPLY(tmp2, FIX_3_072711026);        /*  c1+c3+c5-c7 */
    tmp1 += z1 + z3;
    tmp2 += z1 + z2;

    /* Final output stage: inputs are tmp10..tmp13, tmp0..tmp3 */

    // outptr[0] = range_limit[(int)RIGHT_SHIFT(tmp10 + tmp3,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[7] = range_limit[(int)RIGHT_SHIFT(tmp10 - tmp3,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[1] = range_limit[(int)RIGHT_SHIFT(tmp11 + tmp2,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[6] = range_limit[(int)RIGHT_SHIFT(tmp11 - tmp2,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[2] = range_limit[(int)RIGHT_SHIFT(tmp12 + tmp1,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[5] = range_limit[(int)RIGHT_SHIFT(tmp12 - tmp1,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[3] = range_limit[(int)RIGHT_SHIFT(tmp13 + tmp0,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    // outptr[4] = range_limit[(int)RIGHT_SHIFT(tmp13 - tmp0,
    //                                          CONST_BITS + PASS1_BITS + 3) &
    //                         RANGE_MASK];
    outptr_false[0] = range_limit[(int)RIGHT_SHIFT(tmp10 + tmp3,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[7] = range_limit[(int)RIGHT_SHIFT(tmp10 - tmp3,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[1] = range_limit[(int)RIGHT_SHIFT(tmp11 + tmp2,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[6] = range_limit[(int)RIGHT_SHIFT(tmp11 - tmp2,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[2] = range_limit[(int)RIGHT_SHIFT(tmp12 + tmp1,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[5] = range_limit[(int)RIGHT_SHIFT(tmp12 - tmp1,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[3] = range_limit[(int)RIGHT_SHIFT(tmp13 + tmp0,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];
    outptr_false[4] = range_limit[(int)RIGHT_SHIFT(tmp13 - tmp0,
                                             CONST_BITS + PASS1_BITS + 3) &
                            RANGE_MASK];

    // wsptr += DCTSIZE;		/* advance pointer to next row */

#ifndef NO_ZERO_ROW_TEST
    }
#endif

__asm__ volatile(".byte 0x2e,0x90":::);
__asm__ volatile(".byte 0x2e,0x90":::);
__asm__ volatile(".byte 0x2e,0x90":::);
__asm__ volatile(".byte 0x2e,0x90":::);
__asm__ volatile(".byte 0x2e,0x90":::);
__asm__ volatile(".byte 0x2e,0x90":::);
__asm__ volatile(".byte 0x2e,0x90":::);

    int condition = false;
#ifndef NO_ZERO_ROW_TEST
    condition = wsptr[1] == 0 &&
                wsptr[2] == 0 &&
                wsptr[3] == 0 &&
                wsptr[4] == 0 &&
                wsptr[5] == 0 &&
                wsptr[6] == 0 &&
                wsptr[7] == 0;
#endif
    outptr[0] = (unsigned char)cmov(condition , outptr_true[0], outptr_false[0]);
    outptr[1] = (unsigned char)cmov(condition , outptr_true[1], outptr_false[1]);
    outptr[2] = (unsigned char)cmov(condition , outptr_true[2], outptr_false[2]);
    outptr[3] = (unsigned char)cmov(condition , outptr_true[3], outptr_false[3]);
    outptr[4] = (unsigned char)cmov(condition , outptr_true[4], outptr_false[4]);
    outptr[5] = (unsigned char)cmov(condition , outptr_true[5], outptr_false[5]);
    outptr[6] = (unsigned char)cmov(condition , outptr_true[6], outptr_false[6]);
    outptr[7] = (unsigned char)cmov(condition , outptr_true[7], outptr_false[7]);

    // printf("Iteration %d (%s), outptr[0]=%c\n",ctr,condition ? "true" : "false", outptr[0]);
    // printf("Iteration %d (%s), outptr[1]=%c\n",ctr,condition ? "true" : "false", outptr[1]);
    // printf("Iteration %d (%s), outptr[2]=%c\n",ctr,condition ? "true" : "false", outptr[2]);
    // printf("Iteration %d (%s), outptr[3]=%c\n",ctr,condition ? "true" : "false", outptr[3]);
    // printf("Iteration %d (%s), outptr[4]=%c\n",ctr,condition ? "true" : "false", outptr[4]);
    // printf("Iteration %d (%s), outptr[5]=%c\n",ctr,condition ? "true" : "false", outptr[5]);
    // printf("Iteration %d (%s), outptr[6]=%c\n",ctr,condition ? "true" : "false", outptr[6]);
    // printf("Iteration %d (%s), outptr[7]=%c\n",ctr,condition ? "true" : "false", outptr[7]);

    wsptr += DCTSIZE;		/* advance pointer to next row */
  }

  // printf("End of %s\n",__func__);
  // fflush(stdout);
  // assert(0);
  printf("1.");
  fflush(stdout);
}

#endif /* DCT_ISLOW_SUPPORTED */