
#ifndef __CMOV_H_
#define __CMOV_H_

#include <assert.h>
#include <stdio.h>

static int cmov(int pred, int t_val, int f_val)
{
    int result = 666;
    assert(pred == 0 || pred == 1);
    // printf("(asm before) pred: %s (%d) , t_val: %d, f_val: %d, result: %d\n",pred ? "true" : "false", pred ,t_val,f_val, result);
    // __asm__ volatile("movl %2, %0;\n"
    //                  "test %1, %1;\n"
    //                  "cmovz %3, %0;\n"
    //                  : "=r"(result)
    //                  : "r"(pred), "r"(t_val), "r"(f_val)
    //                  : "cc");
    // __asm__ volatile("movl %2, %0;\n"
    //                  "cmpl $0, %1;\n"
    //                  "cmovne %3, %0;\n"
    //                  : "=r"(result)
    //                  : "r"(pred), "r"(t_val), "r"(f_val)
    //                  : "cc");

    __asm__ volatile("cmpl $0, %1\n\t"
                     "cmovne %2, %%eax\n\t"
                     "cmove %3, %%eax\n\t"
                     "movl %%eax, %0\n\t"
                     : "=r"(result)
                     : "r"(pred), "r"(t_val), "r"(f_val)
                     : "cc", "eax");

    // 64
    // __asm__ volatile("cmpl $0, %1\n\t"
    //                  "cmovne %2, %%rax\n\t"
    //                  "cmove %3, %%rax\n\t"
    //                  "movl %%rax, %0\n\t"
    //                  : "=r"(result)
    //                  : "r"(pred), "r"(t_val), "r"(f_val)
    //                  : "cc", "rax");

    // 16
    // __asm__ volatile("cmpl $0, %1\n\t"
    //                  "cmovne %2, %%ax\n\t"
    //                  "cmove %3, %%ax\n\t"
    //                  "movl %%ax, %0\n\t"
    //                  : "=r"(result)
    //                  : "r"(pred), "r"(t_val), "r"(f_val)
    //                  : "cc", "ax");

    // 8
    // __asm__ volatile("cmpl $0, %1\n\t"
    //                  "cmovne %2, %%al\n\t"
    //                  "cmove %3, %%al\n\t"
    //                  "movl %%ax, %0\n\t"
    //                  : "=r"(result)
    //                  : "r"(pred), "r"(t_val), "r"(f_val)
    //                  : "cc", "al");

    // printf("(asm after) pred: %s (%d) , t_val: %d, f_val: %d, result: %d\n",pred ? "true" : "false", pred ,t_val,f_val, result);

    return result;
}

#endif