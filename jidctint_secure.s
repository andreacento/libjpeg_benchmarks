	.file	"jidctint.c"
	.section	.rodata
.LC0:
	.string	"cmov.h"
.LC1:
	.string	"pred == 0 || pred == 1"
	.text
	.type	cmov, @function
cmov:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movl	%esi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movl	$666, -4(%rbp)
	cmpl	$0, -20(%rbp)
	je	.L2
	cmpl	$1, -20(%rbp)
	je	.L2
	movl	$__PRETTY_FUNCTION__.4216, %ecx
	movl	$11, %edx
	movl	$.LC0, %esi
	movl	$.LC1, %edi
	call	__assert_fail
.L2:
	movl	-20(%rbp), %edx
	movl	-24(%rbp), %ecx
	movl	-28(%rbp), %esi
#APP
# 26 "cmov.h" 1
	cmpl $0, %edx
	cmovne %ecx, %eax
	cmove %esi, %eax
	movl %eax, %edx

# 0 "" 2
#NO_APP
	movl	%edx, -4(%rbp)
	movl	-4(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	cmov, .-cmov
	.section	.rodata
.LC2:
	.string	"1."
	.text
	.globl	jpeg_idct_islow
	.type	jpeg_idct_islow, @function
jpeg_idct_islow:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$1032, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -1000(%rbp)
	movq	%rsi, -1008(%rbp)
	movq	%rdx, -1016(%rbp)
	movq	%rcx, -1024(%rbp)
	movl	%r8d, -1028(%rbp)
	movq	-1000(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -72(%rbp)
	movq	-1016(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-1008(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -32(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -40(%rbp)
	leaq	-704(%rbp), %rax
	movq	%rax, -56(%rbp)
	leaq	-960(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	$8, -44(%rbp)
	jmp	.L5
.L10:
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L6
	movq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	sall	$2, %eax
	movl	%eax, -76(%rbp)
	movq	-56(%rbp), %rax
	movl	-76(%rbp), %edx
	movl	%edx, (%rax)
	movq	-56(%rbp), %rax
	leaq	32(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	64(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	96(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	128(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	160(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	192(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-56(%rbp), %rax
	leaq	224(%rax), %rdx
	movl	-76(%rbp), %eax
	movl	%eax, (%rdx)
	jmp	.L7
.L6:
	movq	-24(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	salq	$13, -88(%rbp)
	salq	$13, -96(%rbp)
	addq	$1024, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-112(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-24(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-32(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-104(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-64(%rbp), %rax
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-64(%rbp), %rax
	addq	$32, %rax
	movq	-160(%rbp), %rcx
	movq	-128(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-64(%rbp), %rax
	addq	$64, %rax
	movq	-168(%rbp), %rcx
	movq	-112(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-64(%rbp), %rax
	addq	$96, %rax
	movq	-152(%rbp), %rcx
	movq	-104(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-64(%rbp), %rax
	leaq	128(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
.L7:
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movq	-24(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movq	-24(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L8
	movl	$1, %eax
	jmp	.L9
.L8:
	movl	$0, %eax
.L9:
	movl	%eax, -172(%rbp)
	movq	-64(%rbp), %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, %edx
	movq	-40(%rbp), %rax
	movl	%edx, (%rax)
	movq	-40(%rbp), %rax
	leaq	32(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	64(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	96(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	128(%rax), %rbx
	movq	-64(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	160(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	192(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	movq	-40(%rbp), %rax
	leaq	224(%rax), %rbx
	movq	-64(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %edx
	movq	-56(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %ecx
	movl	-172(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, (%rbx)
	addq	$4, -64(%rbp)
	addq	$4, -56(%rbp)
	addq	$2, -24(%rbp)
	addq	$4, -32(%rbp)
	addq	$4, -40(%rbp)
	subl	$1, -44(%rbp)
.L5:
	cmpl	$0, -44(%rbp)
	jg	.L10
	leaq	-448(%rbp), %rax
	movq	%rax, -40(%rbp)
	movl	$0, -44(%rbp)
	jmp	.L11
.L16:
	movl	-44(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-1024(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-1028(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-40(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -88(%rbp)
	movq	-40(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L12
	movq	-88(%rbp), %rax
	sarq	$5, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -185(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -976(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -975(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -974(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -973(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -972(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -971(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -970(%rbp)
	movzbl	-185(%rbp), %eax
	movb	%al, -969(%rbp)
	jmp	.L13
.L12:
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -112(%rbp)
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-40(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-112(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-40(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-40(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-112(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-104(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-136(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -992(%rbp)
	movq	-144(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -985(%rbp)
	movq	-160(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -991(%rbp)
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -986(%rbp)
	movq	-168(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -990(%rbp)
	movq	-168(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -987(%rbp)
	movq	-152(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -989(%rbp)
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movb	%al, -988(%rbp)
.L13:
	movl	$0, -192(%rbp)
	movq	-40(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-40(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-40(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-40(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-40(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movq	-40(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L14
	movl	$1, %eax
	jmp	.L15
.L14:
	movl	$0, %eax
.L15:
	movl	%eax, -192(%rbp)
	movzbl	-992(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-976(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movl	%eax, %edx
	movq	-184(%rbp), %rax
	movb	%dl, (%rax)
	movq	-184(%rbp), %rax
	leaq	1(%rax), %rbx
	movzbl	-991(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-975(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-184(%rbp), %rax
	leaq	2(%rax), %rbx
	movzbl	-990(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-974(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-184(%rbp), %rax
	leaq	3(%rax), %rbx
	movzbl	-989(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-973(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-184(%rbp), %rax
	leaq	4(%rax), %rbx
	movzbl	-988(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-972(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-184(%rbp), %rax
	leaq	5(%rax), %rbx
	movzbl	-987(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-971(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-184(%rbp), %rax
	leaq	6(%rax), %rbx
	movzbl	-986(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-970(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	movq	-184(%rbp), %rax
	leaq	7(%rax), %rbx
	movzbl	-985(%rbp), %eax
	movzbl	%al, %edx
	movzbl	-969(%rbp), %eax
	movzbl	%al, %ecx
	movl	-192(%rbp), %eax
	movl	%ecx, %esi
	movl	%eax, %edi
	call	cmov
	movb	%al, (%rbx)
	addq	$32, -40(%rbp)
	addl	$1, -44(%rbp)
.L11:
	cmpl	$7, -44(%rbp)
	jle	.L16
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movq	stdout(%rip), %rax
	movq	%rax, %rdi
	call	fflush
	addq	$1032, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
jpeg_idct_7x7:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$256, %rsp
	movq	%rdi, -344(%rbp)
	movq	%rsi, -352(%rbp)
	movq	%rdx, -360(%rbp)
	movq	%rcx, -368(%rbp)
	movl	%r8d, -372(%rbp)
	movq	-344(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-352(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L18
.L19:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-15083, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	subq	%rax, -64(%rbp)
	movq	-104(%rbp), %rax
	imulq	$10438, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-637, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-20239, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11585, %rax, %rax
	addq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7663, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$1395, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11295, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$5027, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$15326, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	168(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movq	-96(%rbp), %rcx
	movq	-112(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	140(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$56, %rax
	movq	-88(%rbp), %rcx
	movq	-120(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	112(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$84, %rax
	movq	-48(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L18:
	cmpl	$6, -28(%rbp)
	jle	.L19
	leaq	-336(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L20
.L21:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-368(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-372(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-15083, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	subq	%rax, -64(%rbp)
	movq	-104(%rbp), %rax
	imulq	$10438, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-637, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-20239, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11585, %rax, %rax
	addq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7663, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$1395, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11295, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$5027, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$15326, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-128(%rbp), %rax
	movb	%dl, (%rax)
	movq	-128(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-96(%rbp), %rcx
	movq	-112(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-120(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$28, -24(%rbp)
	addl	$1, -28(%rbp)
.L20:
	cmpl	$6, -28(%rbp)
	jle	.L21
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	jpeg_idct_7x7, .-jpeg_idct_7x7
jpeg_idct_6x6:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movq	%rdi, -280(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movl	%r8d, -308(%rbp)
	movq	-280(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-296(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-288(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L23
.L24:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$2998, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rax
	subq	-104(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	120(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movq	-80(%rbp), %rdx
	movl	%edx, %ecx
	movq	-72(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-80(%rbp), %rdx
	movl	%edx, %ecx
	movq	-72(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	72(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L23:
	cmpl	$5, -28(%rbp)
	jle	.L24
	leaq	-272(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L25
.L26:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-304(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-308(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$2998, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rax
	subq	-104(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-120(%rbp), %rax
	movb	%dl, (%rax)
	movq	-120(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$24, -24(%rbp)
	addl	$1, -28(%rbp)
.L25:
	cmpl	$5, -28(%rbp)
	jle	.L26
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
jpeg_idct_5x5:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$144, %rsp
	movq	%rdi, -232(%rbp)
	movq	%rsi, -240(%rbp)
	movq	%rdx, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movl	%r8d, -260(%rbp)
	movq	-232(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-240(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-224(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L28
.L29:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6476, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2896, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-80(%rbp), %rax
	salq	$2, %rax
	subq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-96(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movq	-104(%rbp), %rcx
	movq	-64(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	60(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$40, %rax
	movq	-48(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L28:
	cmpl	$4, -28(%rbp)
	jle	.L29
	leaq	-224(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L30
.L31:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-256(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-260(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6476, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2896, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-80(%rbp), %rax
	salq	$2, %rax
	subq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-96(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-112(%rbp), %rax
	movb	%dl, (%rax)
	movq	-112(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-64(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$20, -24(%rbp)
	addl	$1, -28(%rbp)
.L30:
	cmpl	$4, -28(%rbp)
	jle	.L31
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
jpeg_idct_4x4:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movl	%r8d, -212(%rbp)
	movq	-184(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-192(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L33
.L34:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -96(%rbp)
	addq	$1024, -96(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -48(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	addl	%edx, %eax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movq	-64(%rbp), %rdx
	movl	%edx, %ecx
	movq	-48(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movq	-72(%rbp), %rdx
	movl	%edx, %ecx
	movq	-56(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-72(%rbp), %rdx
	movl	%edx, %ecx
	movq	-56(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L33:
	cmpl	$3, -28(%rbp)
	jle	.L34
	leaq	-176(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L35
.L36:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-208(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-212(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-104(%rbp), %rax
	movb	%dl, (%rax)
	movq	-104(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$16, -24(%rbp)
	addl	$1, -28(%rbp)
.L35:
	cmpl	$3, -28(%rbp)
	jle	.L36
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
jpeg_idct_3x3:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -136(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movl	%r8d, -164(%rbp)
	movq	-136(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-152(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-144(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L38
.L39:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	24(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movq	-56(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L38:
	cmpl	$2, -28(%rbp)
	jle	.L39
	leaq	-128(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L40
.L41:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-160(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-164(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-80(%rbp), %rax
	movb	%dl, (%rax)
	movq	-80(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-80(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$12, -24(%rbp)
	addl	$1, -28(%rbp)
.L40:
	cmpl	$2, -28(%rbp)
	jle	.L41
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
jpeg_idct_2x2:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movl	%r8d, -84(%rbp)
	movq	-56(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -8(%rbp)
	movq	-64(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-72(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -20(%rbp)
	movq	-72(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -24(%rbp)
	addl	$4100, -20(%rbp)
	movl	-20(%rbp), %edx
	movl	-24(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, -28(%rbp)
	movl	-20(%rbp), %eax
	subl	-24(%rbp), %eax
	movl	%eax, -32(%rbp)
	movq	-72(%rbp), %rax
	addq	$2, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -20(%rbp)
	movq	-72(%rbp), %rax
	addq	$18, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$36, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -24(%rbp)
	movl	-20(%rbp), %edx
	movl	-24(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, -36(%rbp)
	movl	-20(%rbp), %eax
	subl	-24(%rbp), %eax
	movl	%eax, -40(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rdx
	movl	-84(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movl	-28(%rbp), %edx
	movl	-36(%rbp), %eax
	addl	%edx, %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	movq	-48(%rbp), %rax
	leaq	1(%rax), %rdx
	movl	-28(%rbp), %eax
	subl	-36(%rbp), %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-8(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-80(%rbp), %rax
	addq	$8, %rax
	movq	(%rax), %rdx
	movl	-84(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movl	-32(%rbp), %edx
	movl	-40(%rbp), %eax
	addl	%edx, %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-48(%rbp), %rax
	movb	%dl, (%rax)
	movq	-48(%rbp), %rax
	leaq	1(%rax), %rdx
	movl	-32(%rbp), %eax
	subl	-40(%rbp), %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-8(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
jpeg_idct_1x1:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%r8d, -68(%rbp)
	movq	-40(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -8(%rbp)
	movq	-48(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-56(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -20(%rbp)
	addl	$4100, -20(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rdx
	movl	-68(%rbp), %eax
	addq	%rax, %rdx
	movl	-20(%rbp), %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-8(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
jpeg_idct_9x9:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$368, %rsp
	movq	%rdi, -456(%rbp)
	movq	%rsi, -464(%rbp)
	movq	%rdx, -472(%rbp)
	movq	%rcx, -480(%rbp)
	movl	%r8d, -484(%rbp)
	movq	-456(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-472(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-464(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L45
.L46:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-96(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-96(%rbp), %rax
	subq	-48(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10887, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	imulq	$8875, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	imulq	$2012, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	subq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -144(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-10033, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7447, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	imulq	$3962, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	subq	-64(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rax
	subq	-144(%rbp), %rax
	imulq	$11409, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	subq	-88(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	subq	-144(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-104(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-128(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-112(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L45:
	cmpl	$7, -28(%rbp)
	jle	.L46
	leaq	-448(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L47
.L48:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-480(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-484(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-96(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-96(%rbp), %rax
	subq	-48(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10887, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	imulq	$8875, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	imulq	$2012, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	subq	-80(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-88(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -144(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-10033, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7447, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	imulq	$3962, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	subq	-64(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rax
	subq	-144(%rbp), %rax
	imulq	$11409, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	subq	-88(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	subq	-144(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-152(%rbp), %rax
	movb	%dl, (%rax)
	movq	-152(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-152(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L47:
	cmpl	$8, -28(%rbp)
	jle	.L48
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
jpeg_idct_10x10:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$416, %rsp
	movq	%rdi, -504(%rbp)
	movq	%rsi, -512(%rbp)
	movq	%rdx, -520(%rbp)
	movq	%rcx, -528(%rbp)
	movl	%r8d, -532(%rbp)
	movq	-504(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-512(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L50
.L51:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$9373, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$3580, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-48(%rbp), %rax
	subq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-88(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	imulq	$2531, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	imulq	$7791, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-152(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11443, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	imulq	$1812, %rax, %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-88(%rbp), %rax
	imulq	$4815, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	-112(%rbp), %rdx
	salq	$12, %rdx
	subq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	subq	-112(%rbp), %rax
	subq	-48(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -104(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10323, %rax, %rax
	subq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5260, %rax, %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-136(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-96(%rbp), %rdx
	movl	%edx, %ecx
	movq	-104(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$224, %rax
	movq	-96(%rbp), %rdx
	movl	%edx, %ecx
	movq	-104(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-144(%rbp), %rcx
	movq	-112(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L50:
	cmpl	$7, -28(%rbp)
	jle	.L51
	leaq	-496(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L52
.L53:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-528(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-532(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$9373, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$3580, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-48(%rbp), %rax
	subq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-88(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	imulq	$2531, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	imulq	$7791, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11443, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	imulq	$1812, %rax, %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-88(%rbp), %rax
	imulq	$4815, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	-112(%rbp), %rdx
	salq	$12, %rdx
	subq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	subq	-112(%rbp), %rax
	salq	$13, %rax
	subq	-48(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10323, %rax, %rax
	subq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5260, %rax, %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-168(%rbp), %rax
	movb	%dl, (%rax)
	movq	-168(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-112(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L52:
	cmpl	$9, -28(%rbp)
	jle	.L53
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
jpeg_idct_11x11:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$448, %rsp
	movq	%rdi, -536(%rbp)
	movq	%rsi, -544(%rbp)
	movq	%rdx, -552(%rbp)
	movq	%rcx, -560(%rbp)
	movl	%r8d, -564(%rbp)
	movq	-536(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-552(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-544(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-528(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L55
.L56:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	imulq	$20862, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$3529, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-9467, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-64(%rbp), %rax
	subq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	imulq	$11116, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-14924, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-72(%rbp), %rax
	imulq	$17333, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-12399, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-6461, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-64(%rbp), %rax
	imulq	$15929, %rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-11395, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-11585, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$3264, %rax, %rax
	movq	%rax, -144(%rbp)
	movq	-136(%rbp), %rax
	imulq	$7274, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$5492, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-56(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$3000, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-136(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-160(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-7562, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-9527, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$16984, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-9766, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -152(%rbp)
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-14731, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	-96(%rbp), %rax
	imulq	$17223, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -160(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-12019, %rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$8203, %rax, %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	imulq	$-13802, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -144(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	320(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-120(%rbp), %rcx
	movq	-136(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-152(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-88(%rbp), %rcx
	movq	-160(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-104(%rbp), %rcx
	movq	-144(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-144(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-112(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L55:
	cmpl	$7, -28(%rbp)
	jle	.L56
	leaq	-528(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L57
.L58:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-560(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-564(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	imulq	$20862, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$3529, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-9467, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-64(%rbp), %rax
	subq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	imulq	$11116, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-14924, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-72(%rbp), %rax
	imulq	$17333, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-12399, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-6461, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-64(%rbp), %rax
	imulq	$15929, %rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-11395, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-11585, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$3264, %rax, %rax
	movq	%rax, -144(%rbp)
	movq	-136(%rbp), %rax
	imulq	$7274, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$5492, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-56(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$3000, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-136(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-160(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-7562, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-9527, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$16984, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-9766, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -152(%rbp)
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-14731, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	-96(%rbp), %rax
	imulq	$17223, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -160(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-12019, %rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$8203, %rax, %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	imulq	$-13802, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -144(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-168(%rbp), %rax
	movb	%dl, (%rax)
	movq	-168(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-120(%rbp), %rcx
	movq	-136(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-152(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-144(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-144(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L57:
	cmpl	$10, -28(%rbp)
	jle	.L58
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
jpeg_idct_12x12:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$480, %rsp
	movq	%rdi, -568(%rbp)
	movq	%rsi, -576(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rcx, -592(%rbp)
	movl	%r8d, -596(%rbp)
	movq	-568(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-584(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-576(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-560(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L60
.L61:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	imulq	$11190, %rax, %rax
	movq	%rax, -56(%rbp)
	salq	$13, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	salq	$13, -88(%rbp)
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-4433, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7053, %rax, %rax
	movq	%rax, -160(%rbp)
	movq	-64(%rbp), %rax
	imulq	$2139, %rax, %rdx
	movq	-160(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movq	-80(%rbp), %rdx
	movq	%rdx, %rax
	salq	$3, %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-8565, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-12112, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-160(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$12998, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-80(%rbp), %rax
	imulq	$-5540, %rax, %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-16244, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -160(%rbp)
	movq	-56(%rbp), %rax
	subq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	352(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-104(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	320(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-136(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-144(%rbp), %rcx
	movq	-168(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-168(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-112(%rbp), %rcx
	movq	-152(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-152(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L60:
	cmpl	$7, -28(%rbp)
	jle	.L61
	leaq	-560(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L62
.L63:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-592(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-596(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	imulq	$11190, %rax, %rax
	movq	%rax, -56(%rbp)
	salq	$13, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	salq	$13, -88(%rbp)
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-4433, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7053, %rax, %rax
	movq	%rax, -160(%rbp)
	movq	-64(%rbp), %rax
	imulq	$2139, %rax, %rdx
	movq	-160(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movq	-80(%rbp), %rdx
	movq	%rdx, %rax
	salq	$3, %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-8565, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-12112, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-160(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$12998, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-80(%rbp), %rax
	imulq	$-5540, %rax, %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-16244, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -160(%rbp)
	movq	-56(%rbp), %rax
	subq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-176(%rbp), %rax
	movb	%dl, (%rax)
	movq	-176(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-168(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-168(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-112(%rbp), %rcx
	movq	-152(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-152(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L62:
	cmpl	$11, -28(%rbp)
	jle	.L63
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
jpeg_idct_13x13:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$528, %rsp
	movq	%rdi, -616(%rbp)
	movq	%rsi, -624(%rbp)
	movq	%rdx, -632(%rbp)
	movq	%rcx, -640(%rbp)
	movl	%r8d, -644(%rbp)
	movq	-616(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-632(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-624(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-608(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L65
.L66:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rax
	imulq	$9465, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	imulq	$793, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$11249, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	imulq	$4108, %rax, %rax
	subq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, %rax
	salq	$3, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %rax
	salq	$5, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	imulq	$3989, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$8672, %rax, %rax
	subq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-10258, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	imulq	$3570, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	imulq	$7678, %rax, %rax
	subq	-48(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-1396, %rax, %rax
	subq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-6581, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	subq	-104(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$11585, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10832, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9534, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	imulq	$7682, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-16549, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-2773, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-56(%rbp), %rax
	imulq	$6859, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-12879, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-9534, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$18068, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-5384, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-176(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-168(%rbp), %rax
	imulq	$2773, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2611, %rax, %rdx
	movq	-168(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-3818, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-64(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$7682, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	addq	%rax, -176(%rbp)
	movq	-64(%rbp), %rax
	imulq	$3150, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$-14273, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	384(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-128(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	352(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-120(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	320(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-152(%rbp), %rcx
	movq	-176(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-176(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-168(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$192, %rax
	movq	-160(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L65:
	cmpl	$7, -28(%rbp)
	jle	.L66
	leaq	-608(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L67
.L68:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-640(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-644(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rax
	imulq	$9465, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	imulq	$793, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$11249, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	imulq	$4108, %rax, %rax
	subq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, %rax
	salq	$3, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	addq	%rdx, %rax
	salq	$5, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	imulq	$3989, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$8672, %rax, %rax
	subq	-96(%rbp), %rax
	movq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-10258, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	imulq	$3570, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	imulq	$7678, %rax, %rax
	subq	-48(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-1396, %rax, %rax
	subq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-6581, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	subq	-104(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$11585, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10832, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9534, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	imulq	$7682, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-16549, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-2773, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-56(%rbp), %rax
	imulq	$6859, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-12879, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-9534, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$18068, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-5384, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-176(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-168(%rbp), %rax
	imulq	$2773, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2611, %rax, %rdx
	movq	-168(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-3818, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-64(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$7682, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	addq	%rax, -176(%rbp)
	movq	-64(%rbp), %rax
	imulq	$3150, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$-14273, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-184(%rbp), %rax
	movb	%dl, (%rax)
	movq	-184(%rbp), %rax
	leaq	12(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-120(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-104(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-176(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-176(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-168(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-168(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-184(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L67:
	cmpl	$12, -28(%rbp)
	jle	.L68
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
jpeg_idct_14x14:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$560, %rsp
	movq	%rdi, -648(%rbp)
	movq	%rsi, -656(%rbp)
	movq	%rdx, -664(%rbp)
	movq	%rcx, -672(%rbp)
	movl	%r8d, -676(%rbp)
	movq	-648(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-664(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-656(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-640(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L70
.L71:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10438, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	subq	-56(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-48(%rbp), %rax
	subq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9058, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2237, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-14084, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rax
	imulq	$5027, %rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-11295, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-96(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -112(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10935, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	imulq	$9810, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-9232, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-120(%rbp), %rax
	imulq	$6164, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-8693, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-64(%rbp), %rax
	subq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	imulq	$3826, %rax, %rax
	subq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	addq	%rax, -184(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -48(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-1297, %rax, %rax
	subq	-112(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-3474, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-19447, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$11512, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$-13850, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5529, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -112(%rbp)
	movq	-136(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	416(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-152(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	384(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	352(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-104(%rbp), %rdx
	movl	%edx, %ecx
	movq	-112(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$320, %rax
	movq	-104(%rbp), %rdx
	movl	%edx, %ecx
	movq	-112(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-176(%rbp), %rcx
	movq	-120(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-160(%rbp), %rcx
	movq	-128(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$192, %rax
	movq	-144(%rbp), %rcx
	movq	-184(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-184(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L70:
	cmpl	$7, -28(%rbp)
	jle	.L71
	leaq	-640(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L72
.L73:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-672(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-676(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10438, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	subq	-56(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-48(%rbp), %rax
	subq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9058, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2237, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-14084, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rax
	imulq	$5027, %rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-11295, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-96(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	salq	$13, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10935, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	imulq	$9810, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-9232, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-120(%rbp), %rax
	imulq	$6164, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-8693, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-64(%rbp), %rax
	subq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	imulq	$3826, %rax, %rax
	subq	-56(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	addq	%rax, -184(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-1297, %rax, %rax
	subq	-56(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-3474, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-19447, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$11512, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$-13850, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5529, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-136(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-192(%rbp), %rax
	movb	%dl, (%rax)
	movq	-192(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	12(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-176(%rbp), %rcx
	movq	-120(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-160(%rbp), %rcx
	movq	-128(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-184(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-184(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L72:
	cmpl	$13, -28(%rbp)
	jle	.L73
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
jpeg_idct_15x15:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$608, %rsp
	movq	%rdi, -696(%rbp)
	movq	%rsi, -704(%rbp)
	movq	%rdx, -712(%rbp)
	movq	%rcx, -720(%rbp)
	movl	%r8d, -724(%rbp)
	movq	-696(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-712(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-704(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-688(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L75
.L76:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$3580, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$9373, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-80(%rbp), %rax
	addq	%rax, %rax
	subq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10958, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$374, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$11795, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-104(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	imulq	$4482, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$3271, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	subq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-64(%rbp), %rax
	imulq	$6476, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$2896, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-48(%rbp), %rax
	subq	-88(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-48(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-6810, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-11018, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$11522, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	imulq	$20131, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	subq	-176(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-9113, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10033, %rax, %rax
	subq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4712, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rax
	imulq	$3897, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	subq	-64(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-7121, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -176(%rbp)
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	448(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-144(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	416(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-160(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	384(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	352(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-152(%rbp), %rcx
	movq	-184(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	320(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-184(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-128(%rbp), %rcx
	movq	-176(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-176(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$192, %rax
	movq	-136(%rbp), %rcx
	movq	-192(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-192(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$224, %rax
	movq	-168(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L75:
	cmpl	$7, -28(%rbp)
	jle	.L76
	leaq	-688(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L77
.L78:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-720(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-724(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -200(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$3580, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$9373, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-80(%rbp), %rax
	addq	%rax, %rax
	subq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10958, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$374, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$11795, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-104(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	imulq	$4482, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$3271, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	subq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-64(%rbp), %rax
	imulq	$6476, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$2896, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-48(%rbp), %rax
	subq	-88(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-48(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-176(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-6810, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-11018, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$11522, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	imulq	$20131, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	subq	-176(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-9113, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10033, %rax, %rax
	subq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4712, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rax
	imulq	$3897, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	subq	-64(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-7121, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -176(%rbp)
	movq	-112(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-200(%rbp), %rax
	movb	%dl, (%rax)
	movq	-200(%rbp), %rax
	leaq	14(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-160(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	12(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-184(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-184(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-176(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-176(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-192(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-192(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-200(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-168(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L77:
	cmpl	$14, -28(%rbp)
	jle	.L78
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
jpeg_idct_16x16:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$640, %rsp
	movq	%rdi, -728(%rbp)
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%rcx, -752(%rbp)
	movl	%r8d, -756(%rbp)
	movq	-728(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-744(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-736(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-720(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L80
.L81:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	imulq	$2260, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rax
	imulq	$11363, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$20995, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	imulq	$7373, %rax, %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-4926, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-4176, %rax, %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-80(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-104(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-88(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-88(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$11086, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10217, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$8956, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rax
	subq	-128(%rbp), %rax
	imulq	$7350, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	imulq	$5461, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-136(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-18730, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-15038, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$1136, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rax
	imulq	$589, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-9222, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -72(%rbp)
	movq	-120(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	$11529, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-6278, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16154, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-5461, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-128(%rbp), %rax
	imulq	$8728, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-10217, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25733, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11086, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	-128(%rbp), %rax
	subq	-120(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-144(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	480(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-160(%rbp), %rcx
	movq	-64(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	448(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-176(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	416(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-192(%rbp), %rcx
	movq	-136(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	384(%rax), %rdx
	movq	-192(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-200(%rbp), %rcx
	movq	-80(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	352(%rax), %rdx
	movq	-200(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-184(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	320(%rax), %rdx
	movq	-184(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$192, %rax
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$224, %rax
	movq	-152(%rbp), %rcx
	movq	-104(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L80:
	cmpl	$7, -28(%rbp)
	jle	.L81
	leaq	-720(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L82
.L83:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-752(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-756(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -208(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	imulq	$2260, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rax
	imulq	$11363, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$20995, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	imulq	$7373, %rax, %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-4926, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-4176, %rax, %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-80(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-104(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-88(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-88(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$11086, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10217, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$8956, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rax
	subq	-128(%rbp), %rax
	imulq	$7350, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	imulq	$5461, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-136(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-18730, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-15038, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$1136, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rax
	imulq	$589, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-9222, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -72(%rbp)
	movq	-120(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	$11529, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-6278, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16154, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-5461, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-128(%rbp), %rax
	imulq	$8728, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-10217, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25733, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11086, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	-128(%rbp), %rax
	subq	-120(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-144(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-208(%rbp), %rax
	movb	%dl, (%rax)
	movq	-208(%rbp), %rax
	leaq	15(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-160(%rbp), %rcx
	movq	-64(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	14(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-176(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-192(%rbp), %rcx
	movq	-136(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	12(%rax), %rdx
	movq	-192(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-200(%rbp), %rcx
	movq	-80(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-200(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-184(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-184(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-104(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L82:
	cmpl	$15, -28(%rbp)
	jle	.L83
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
jpeg_idct_16x8:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$400, %rsp
	movq	%rdi, -488(%rbp)
	movq	%rsi, -496(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%rcx, -512(%rbp)
	movl	%r8d, -516(%rbp)
	movq	-488(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-496(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-480(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$8, -28(%rbp)
	jmp	.L85
.L88:
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L86
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	sall	$2, %eax
	movl	%eax, -44(%rbp)
	movq	-24(%rbp), %rax
	movl	-44(%rbp), %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	32(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	64(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	96(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	128(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
	jmp	.L87
.L86:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	salq	$13, -56(%rbp)
	salq	$13, -64(%rbp)
	addq	$1024, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-72(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -72(%rbp)
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-80(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-128(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-120(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	128(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L87:
	subl	$1, -28(%rbp)
.L85:
	cmpl	$0, -28(%rbp)
	jg	.L88
	leaq	-480(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L89
.L90:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-512(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-516(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -72(%rbp)
	salq	$13, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$2260, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11363, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$20995, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$7373, %rax, %rdx
	movq	-152(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-4926, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-4176, %rax, %rdx
	movq	-152(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-112(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-112(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-136(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-120(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-120(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-128(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -208(%rbp)
	movq	-128(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$11086, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rax
	imulq	$10217, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rdx, %rax
	imulq	$8956, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	subq	-152(%rbp), %rax
	imulq	$7350, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$5461, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rax, %rdx
	movq	-88(%rbp), %rax
	imulq	$-18730, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rax, %rdx
	movq	-136(%rbp), %rax
	addq	%rax, %rdx
	movq	-88(%rbp), %rax
	imulq	$-15038, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$1136, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$589, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-9222, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	subq	-56(%rbp), %rax
	imulq	$11529, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-6278, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-56(%rbp), %rax
	imulq	$16154, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-152(%rbp), %rax
	addq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-5461, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, -80(%rbp)
	movq	-152(%rbp), %rax
	imulq	$8728, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-10217, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-152(%rbp), %rax
	imulq	$25733, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	-64(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11086, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-152(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -128(%rbp)
	movq	-160(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-144(%rbp), %rax
	movb	%dl, (%rax)
	movq	-144(%rbp), %rax
	leaq	15(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-176(%rbp), %rcx
	movq	-80(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	14(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-192(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-192(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-208(%rbp), %rcx
	movq	-104(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	12(%rax), %rdx
	movq	-208(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-216(%rbp), %rcx
	movq	-112(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-216(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-200(%rbp), %rcx
	movq	-128(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-200(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-184(%rbp), %rcx
	movq	-136(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-184(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-168(%rbp), %rcx
	movq	-120(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L89:
	cmpl	$7, -28(%rbp)
	jle	.L90
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
jpeg_idct_14x7:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$336, %rsp
	movq	%rdi, -424(%rbp)
	movq	%rsi, -432(%rbp)
	movq	%rdx, -440(%rbp)
	movq	%rcx, -448(%rbp)
	movl	%r8d, -452(%rbp)
	movq	-424(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-432(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-416(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L92
.L93:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-15083, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	subq	%rax, -64(%rbp)
	movq	-104(%rbp), %rax
	imulq	$10438, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-637, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-20239, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11585, %rax, %rax
	addq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7663, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$1395, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11295, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$5027, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$15326, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-96(%rbp), %rcx
	movq	-112(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-88(%rbp), %rcx
	movq	-120(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	128(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-48(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L92:
	cmpl	$7, -28(%rbp)
	jle	.L93
	leaq	-416(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L94
.L95:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-448(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-452(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -56(%rbp)
	salq	$13, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	imulq	$10438, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-136(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-136(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	subq	-136(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-56(%rbp), %rax
	subq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9058, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	imulq	$2237, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-14084, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5027, %rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-11295, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-104(%rbp), %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-104(%rbp), %rax
	subq	-144(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-112(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	subq	-152(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-120(%rbp), %rdx
	movq	-160(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	subq	-160(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -136(%rbp)
	salq	$13, -136(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10935, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-152(%rbp), %rax
	imulq	$9810, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rax, %rdx
	movq	-136(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-9232, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-152(%rbp), %rax
	imulq	$6164, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-8693, %rax, %rdx
	movq	-152(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-64(%rbp), %rax
	subq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$3826, %rax, %rax
	subq	-136(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	addq	%rax, -192(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-1297, %rax, %rax
	subq	-136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-3474, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-19447, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-72(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$11512, %rax, %rax
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$-13850, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -152(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5529, %rax, %rdx
	movq	-144(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -160(%rbp)
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-128(%rbp), %rax
	movb	%dl, (%rax)
	movq	-128(%rbp), %rax
	leaq	13(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-96(%rbp), %rcx
	movq	-112(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	12(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-120(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rcx
	movq	-144(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-48(%rbp), %rax
	subq	-144(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-184(%rbp), %rcx
	movq	-152(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-184(%rbp), %rax
	subq	-152(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-176(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-168(%rbp), %rcx
	movq	-192(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-128(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-192(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L94:
	cmpl	$6, -28(%rbp)
	jle	.L95
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
jpeg_idct_12x6:
.LFB21:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$288, %rsp
	movq	%rdi, -376(%rbp)
	movq	%rsi, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%rcx, -400(%rbp)
	movl	%r8d, -404(%rbp)
	movq	-376(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-384(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L97
.L98:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$2998, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rax
	subq	-104(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-80(%rbp), %rdx
	movl	%edx, %ecx
	movq	-72(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-80(%rbp), %rdx
	movl	%edx, %ecx
	movq	-72(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	96(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L97:
	cmpl	$7, -28(%rbp)
	jle	.L98
	leaq	-368(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L99
.L100:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-400(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-404(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -112(%rbp)
	salq	$13, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	imulq	$11190, %rax, %rax
	movq	%rax, -128(%rbp)
	salq	$13, -96(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -104(%rbp)
	salq	$13, -104(%rbp)
	movq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-128(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-128(%rbp), %rax
	subq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-104(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	imulq	$-4433, %rax, %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7053, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2139, %rax, %rdx
	movq	-168(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movq	-96(%rbp), %rdx
	movq	%rdx, %rax
	salq	$3, %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-8565, %rax, %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	-160(%rbp), %rax
	addq	%rax, %rdx
	movq	-112(%rbp), %rax
	imulq	$-12112, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -56(%rbp)
	movq	-168(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-128(%rbp), %rax
	imulq	$12998, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -176(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-5540, %rax, %rdx
	movq	-160(%rbp), %rax
	addq	%rax, %rdx
	movq	-128(%rbp), %rax
	imulq	$-16244, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-128(%rbp), %rax
	subq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	subq	%rax, -104(%rbp)
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-120(%rbp), %rax
	movb	%dl, (%rax)
	movq	-120(%rbp), %rax
	leaq	11(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	10(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-176(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-176(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-168(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-168(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L99:
	cmpl	$5, -28(%rbp)
	jle	.L100
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21:
jpeg_idct_10x5:
.LFB22:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$240, %rsp
	movq	%rdi, -328(%rbp)
	movq	%rsi, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movl	%r8d, -356(%rbp)
	movq	-328(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-336(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L102
.L103:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6476, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2896, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-80(%rbp), %rax
	salq	$2, %rax
	subq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-96(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	128(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-104(%rbp), %rcx
	movq	-64(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	96(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-48(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L102:
	cmpl	$7, -28(%rbp)
	jle	.L103
	leaq	-320(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L104
.L105:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-352(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-356(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -88(%rbp)
	salq	$13, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	imulq	$9373, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-120(%rbp), %rax
	imulq	$3580, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	subq	-80(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	subq	-80(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-88(%rbp), %rax
	subq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-96(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-104(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-104(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	salq	$13, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-80(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$2531, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-104(%rbp), %rax
	imulq	$7791, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-72(%rbp), %rax
	imulq	$11443, %rax, %rdx
	movq	-80(%rbp), %rax
	addq	%rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	imulq	$1812, %rax, %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-104(%rbp), %rax
	imulq	$4815, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	-56(%rbp), %rdx
	salq	$12, %rdx
	subq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	salq	$13, %rax
	subq	-88(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rax
	imulq	$10323, %rax, %rax
	subq	-80(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rax
	imulq	$5260, %rax, %rax
	subq	-80(%rbp), %rax
	movq	%rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-136(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-112(%rbp), %rax
	movb	%dl, (%rax)
	movq	-112(%rbp), %rax
	leaq	9(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-104(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-48(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-160(%rbp), %rcx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-144(%rbp), %rcx
	movq	-64(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-112(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L104:
	cmpl	$4, -28(%rbp)
	jle	.L105
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22:
jpeg_idct_8x4:
.LFB23:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movq	%rdi, -280(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movl	%r8d, -308(%rbp)
	movq	-280(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-296(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-288(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L107
.L108:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -96(%rbp)
	addq	$1024, -96(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -48(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	movl	%eax, %edx
	movq	-48(%rbp), %rax
	addl	%edx, %eax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-64(%rbp), %rdx
	movl	%edx, %ecx
	movq	-48(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-72(%rbp), %rdx
	movl	%edx, %ecx
	movq	-56(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-72(%rbp), %rdx
	movl	%edx, %ecx
	movq	-56(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L107:
	cmpl	$7, -28(%rbp)
	jle	.L108
	leaq	-272(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L109
.L110:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-304(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-308(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-96(%rbp), %rax
	addq	%rax, -80(%rbp)
	movq	-96(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-120(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -48(%rbp)
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-112(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -112(%rbp)
	movq	-96(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-104(%rbp), %rax
	movb	%dl, (%rax)
	movq	-104(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-128(%rbp), %rcx
	movq	-48(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-104(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L109:
	cmpl	$3, -28(%rbp)
	jle	.L110
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23:
jpeg_idct_6x3:
.LFB24:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movl	%r8d, -228(%rbp)
	movq	-200(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-208(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L112
.L113:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	48(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movq	-56(%rbp), %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L112:
	cmpl	$5, -28(%rbp)
	jle	.L113
	leaq	-192(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L114
.L115:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-224(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-228(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-88(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$2998, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-120(%rbp), %rax
	subq	-112(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-80(%rbp), %rax
	movb	%dl, (%rax)
	movq	-80(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-80(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-80(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-80(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-80(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$24, -24(%rbp)
	addl	$1, -28(%rbp)
.L114:
	cmpl	$2, -28(%rbp)
	jle	.L115
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24:
jpeg_idct_4x2:
.LFB25:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movl	%r8d, -212(%rbp)
	movq	-184(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-192(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L117
.L118:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	leaq	32(%rax), %rdx
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$8, -24(%rbp)
.L117:
	cmpl	$3, -28(%rbp)
	jle	.L118
	leaq	-176(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L119
.L120:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-208(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-212(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	addq	$4100, %rax
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	movq	16(%rax), %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	subq	-72(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	sarq	$16, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-64(%rbp), %rax
	movb	%dl, (%rax)
	movq	-64(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$16, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-64(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$16, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-64(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$16, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L119:
	cmpl	$1, -28(%rbp)
	jle	.L120
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25:
jpeg_idct_2x1:
.LFB26:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%r8d, -68(%rbp)
	movq	-40(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -8(%rbp)
	movq	-48(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rdx
	movl	-68(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -24(%rbp)
	movq	-56(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -28(%rbp)
	addl	$4100, -28(%rbp)
	movq	-56(%rbp), %rax
	addq	$2, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -32(%rbp)
	movl	-28(%rbp), %edx
	movl	-32(%rbp), %eax
	addl	%edx, %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-24(%rbp), %rax
	movb	%dl, (%rax)
	movq	-24(%rbp), %rax
	leaq	1(%rax), %rdx
	movl	-28(%rbp), %eax
	subl	-32(%rbp), %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-8(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE26:
jpeg_idct_8x16:
.LFB27:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$640, %rsp
	movq	%rdi, -728(%rbp)
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%rcx, -752(%rbp)
	movl	%r8d, -756(%rbp)
	movq	-728(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-744(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-736(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-720(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L123
.L124:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	imulq	$2260, %rax, %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rax
	imulq	$11363, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rax
	imulq	$20995, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-56(%rbp), %rax
	imulq	$7373, %rax, %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-4926, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-4176, %rax, %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-80(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-96(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-104(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-88(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-88(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$11086, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10217, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$8956, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rax
	subq	-128(%rbp), %rax
	imulq	$7350, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	imulq	$5461, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-136(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-18730, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-15038, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$1136, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rax
	imulq	$589, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -64(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-9222, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -72(%rbp)
	movq	-120(%rbp), %rax
	subq	-112(%rbp), %rax
	imulq	$11529, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-6278, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-112(%rbp), %rax
	imulq	$16154, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-5461, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-128(%rbp), %rax
	imulq	$8728, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-10217, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-128(%rbp), %rax
	imulq	$25733, %rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -96(%rbp)
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11086, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -72(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -136(%rbp)
	movq	-128(%rbp), %rax
	subq	-120(%rbp), %rax
	imulq	$3363, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -80(%rbp)
	movq	-112(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-144(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	480(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-160(%rbp), %rcx
	movq	-64(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	448(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$64, %rax
	movq	-176(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	416(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-192(%rbp), %rcx
	movq	-136(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	384(%rax), %rdx
	movq	-192(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	subq	$-128, %rax
	movq	-200(%rbp), %rcx
	movq	-80(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	352(%rax), %rdx
	movq	-200(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$160, %rax
	movq	-184(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	320(%rax), %rdx
	movq	-184(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$192, %rax
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	288(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$224, %rax
	movq	-152(%rbp), %rcx
	movq	-104(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	256(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L123:
	cmpl	$7, -28(%rbp)
	jle	.L124
	leaq	-720(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L125
.L126:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-752(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-756(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -208(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rax
	subq	-120(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-64(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -136(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-64(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-112(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -112(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -120(%rbp)
	movq	-48(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-136(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -48(%rbp)
	movq	-56(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -72(%rbp)
	movq	-80(%rbp), %rdx
	movq	-136(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-208(%rbp), %rax
	movb	%dl, (%rax)
	movq	-208(%rbp), %rax
	leaq	7(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-136(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-96(%rbp), %rcx
	movq	-64(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-96(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-48(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-208(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$32, -24(%rbp)
	addl	$1, -28(%rbp)
.L125:
	cmpl	$15, -28(%rbp)
	jle	.L126
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27:
jpeg_idct_7x14:
.LFB28:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$512, %rsp
	movq	%rdi, -600(%rbp)
	movq	%rsi, -608(%rbp)
	movq	%rdx, -616(%rbp)
	movq	%rcx, -624(%rbp)
	movl	%r8d, -628(%rbp)
	movq	-600(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-616(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-608(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L128
.L129:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10438, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	subq	-56(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-48(%rbp), %rax
	subq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9058, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	imulq	$2237, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-14084, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rax
	imulq	$5027, %rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-11295, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-80(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-96(%rbp), %rdx
	movq	-128(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-96(%rbp), %rax
	subq	-128(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -112(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$10935, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rax
	imulq	$9810, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movq	-112(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-9232, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-120(%rbp), %rax
	imulq	$6164, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-8693, %rax, %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -184(%rbp)
	movq	-64(%rbp), %rax
	subq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	imulq	$3826, %rax, %rax
	subq	-112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	addq	%rax, -184(%rbp)
	movq	-56(%rbp), %rax
	addq	%rax, -48(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-1297, %rax, %rax
	subq	-112(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-3474, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-19447, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-72(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$11512, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rax, %rdx
	movq	-72(%rbp), %rax
	imulq	$-13850, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5529, %rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -128(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -112(%rbp)
	movq	-136(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	364(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$28, %rax
	movq	-152(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	336(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$56, %rax
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	308(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$84, %rax
	movq	-104(%rbp), %rdx
	movl	%edx, %ecx
	movq	-112(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$280, %rax
	movq	-104(%rbp), %rdx
	movl	%edx, %ecx
	movq	-112(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$112, %rax
	movq	-176(%rbp), %rcx
	movq	-120(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	252(%rax), %rdx
	movq	-176(%rbp), %rax
	subq	-120(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$140, %rax
	movq	-160(%rbp), %rcx
	movq	-128(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	224(%rax), %rdx
	movq	-160(%rbp), %rax
	subq	-128(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$168, %rax
	movq	-144(%rbp), %rcx
	movq	-184(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	196(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-184(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L128:
	cmpl	$6, -28(%rbp)
	jle	.L129
	leaq	-592(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L130
.L131:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-624(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-628(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -192(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -104(%rbp)
	salq	$13, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	imulq	$7223, %rax, %rax
	movq	%rax, -136(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$2578, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-136(%rbp), %rdx
	movq	-168(%rbp), %rax
	addq	%rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	imulq	$-15083, %rax, %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	subq	%rax, -64(%rbp)
	movq	-80(%rbp), %rax
	imulq	$10438, %rax, %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$-637, %rax, %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -136(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-20239, %rax, %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11585, %rax, %rax
	addq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7663, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	imulq	$1395, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-11295, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	addq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	imulq	$5027, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	imulq	$15326, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-136(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-192(%rbp), %rax
	movb	%dl, (%rax)
	movq	-192(%rbp), %rax
	leaq	6(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-152(%rbp), %rcx
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-152(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-168(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-168(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-192(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$28, -24(%rbp)
	addl	$1, -28(%rbp)
.L130:
	cmpl	$13, -28(%rbp)
	jle	.L131
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28:
jpeg_idct_6x12:
.LFB29:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$384, %rsp
	movq	%rdi, -472(%rbp)
	movq	%rsi, -480(%rbp)
	movq	%rdx, -488(%rbp)
	movq	%rcx, -496(%rbp)
	movl	%r8d, -500(%rbp)
	movq	-472(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-488(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-480(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-464(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L133
.L134:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	imulq	$11190, %rax, %rax
	movq	%rax, -56(%rbp)
	salq	$13, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	salq	$13, -88(%rbp)
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-48(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-56(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-56(%rbp), %rax
	subq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10703, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-4433, %rax, %rax
	movq	%rax, -152(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$7053, %rax, %rax
	movq	%rax, -160(%rbp)
	movq	-64(%rbp), %rax
	imulq	$2139, %rax, %rdx
	movq	-160(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	-72(%rbp), %rax
	leaq	(%rdx,%rax), %rcx
	movq	-80(%rbp), %rdx
	movq	%rdx, %rax
	salq	$3, %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	salq	$8, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%rcx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-8565, %rax, %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-48(%rbp), %rax
	imulq	$-12112, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-160(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$12998, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -168(%rbp)
	movq	-80(%rbp), %rax
	imulq	$-5540, %rax, %rdx
	movq	-152(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	imulq	$-16244, %rax, %rax
	addq	%rdx, %rax
	addq	%rax, -160(%rbp)
	movq	-56(%rbp), %rax
	subq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -152(%rbp)
	movq	-120(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	264(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movq	-104(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	240(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movq	-136(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	216(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$72, %rax
	movq	-144(%rbp), %rcx
	movq	-168(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	192(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-168(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$96, %rax
	movq	-112(%rbp), %rcx
	movq	-152(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	168(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-152(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$120, %rax
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	144(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L133:
	cmpl	$5, -28(%rbp)
	jle	.L134
	leaq	-464(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L135
.L136:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-496(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-500(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -176(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -64(%rbp)
	salq	$13, -64(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rdx
	movq	-120(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	subq	-120(%rbp), %rax
	subq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-72(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-80(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	imulq	$2998, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-88(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	subq	-48(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -72(%rbp)
	movq	-120(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-176(%rbp), %rax
	movb	%dl, (%rax)
	movq	-176(%rbp), %rax
	leaq	5(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-104(%rbp), %rcx
	movq	-72(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-104(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-176(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$24, -24(%rbp)
	addl	$1, -28(%rbp)
.L135:
	cmpl	$11, -28(%rbp)
	jle	.L136
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29:
jpeg_idct_5x10:
.LFB30:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$288, %rsp
	movq	%rdi, -376(%rbp)
	movq	%rsi, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%rcx, -400(%rbp)
	movl	%r8d, -404(%rbp)
	movq	-376(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-384(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L138
.L139:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$9373, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rax
	imulq	$3580, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	subq	-72(%rbp), %rax
	leaq	(%rax,%rax), %rdx
	movq	-48(%rbp), %rax
	subq	%rdx, %rax
	sarq	$11, %rax
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-88(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -136(%rbp)
	movq	-88(%rbp), %rax
	subq	-112(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-72(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	imulq	$2531, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-48(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -152(%rbp)
	movq	-88(%rbp), %rax
	imulq	$7791, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-152(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$11443, %rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-64(%rbp), %rax
	imulq	$1812, %rax, %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-88(%rbp), %rax
	imulq	$4815, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-152(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	-112(%rbp), %rdx
	salq	$12, %rdx
	subq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	subq	-112(%rbp), %rax
	subq	-48(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -104(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10323, %rax, %rax
	subq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	imulq	$5260, %rax, %rax
	subq	-72(%rbp), %rax
	movq	%rax, %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %rdx
	movq	-80(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	180(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$20, %rax
	movq	-136(%rbp), %rcx
	movq	-88(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	160(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$40, %rax
	movq	-96(%rbp), %rdx
	movl	%edx, %ecx
	movq	-104(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$140, %rax
	movq	-96(%rbp), %rdx
	movl	%edx, %ecx
	movq	-104(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$60, %rax
	movq	-144(%rbp), %rcx
	movq	-112(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	120(%rax), %rdx
	movq	-144(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$80, %rax
	movq	-128(%rbp), %rcx
	movq	-160(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	100(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L138:
	cmpl	$4, -28(%rbp)
	jle	.L139
	leaq	-368(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L140
.L141:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-400(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-404(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -168(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -104(%rbp)
	salq	$13, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -160(%rbp)
	movq	-112(%rbp), %rdx
	movq	-160(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6476, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-112(%rbp), %rax
	subq	-160(%rbp), %rax
	imulq	$2896, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -80(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	salq	$2, %rax
	subq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	imulq	$6810, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	imulq	$4209, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-48(%rbp), %rax
	imulq	$-17828, %rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -160(%rbp)
	movq	-80(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-168(%rbp), %rax
	movb	%dl, (%rax)
	movq	-168(%rbp), %rax
	leaq	4(%rax), %rdx
	movq	-80(%rbp), %rax
	subq	-112(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-88(%rbp), %rcx
	movq	-160(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-160(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-168(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-104(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$20, -24(%rbp)
	addl	$1, -28(%rbp)
.L140:
	cmpl	$9, -28(%rbp)
	jle	.L141
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE30:
jpeg_idct_4x8:
.LFB31:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movq	%rdi, -280(%rbp)
	movq	%rsi, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movl	%r8d, -308(%rbp)
	movq	-280(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-296(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-288(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$4, -28(%rbp)
	jmp	.L143
.L146:
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	testw	%ax, %ax
	jne	.L144
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	sall	$2, %eax
	movl	%eax, -44(%rbp)
	movq	-24(%rbp), %rax
	movl	-44(%rbp), %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	16(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	32(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	48(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	64(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	80(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	96(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	leaq	112(%rax), %rdx
	movl	-44(%rbp), %eax
	movl	%eax, (%rdx)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
	jmp	.L145
.L144:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	salq	$13, -56(%rbp)
	salq	$13, -64(%rbp)
	addq	$1024, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-56(%rbp), %rax
	subq	-64(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-8(%rbp), %rax
	addq	$96, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$192, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rax
	subq	-104(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8(%rbp), %rax
	addq	$112, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$224, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$9633, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$-16069, %rax, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-3196, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, -56(%rbp)
	movq	-88(%rbp), %rax
	addq	%rax, -64(%rbp)
	movq	-72(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-7373, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rax
	imulq	$2446, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-104(%rbp), %rax
	imulq	$12299, %rax, %rax
	movq	%rax, -104(%rbp)
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -72(%rbp)
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -104(%rbp)
	movq	-80(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	imulq	$-20995, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rax
	imulq	$16819, %rax, %rax
	movq	%rax, -80(%rbp)
	movq	-96(%rbp), %rax
	imulq	$25172, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -80(%rbp)
	movq	-88(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	addq	%rax, -96(%rbp)
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	112(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-104(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movq	-128(%rbp), %rcx
	movq	-96(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	96(%rax), %rdx
	movq	-128(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$32, %rax
	movq	-136(%rbp), %rcx
	movq	-80(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-80(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movq	-120(%rbp), %rcx
	movq	-72(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	64(%rax), %rdx
	movq	-120(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L145:
	subl	$1, -28(%rbp)
.L143:
	cmpl	$0, -28(%rbp)
	jg	.L146
	leaq	-272(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L147
.L148:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-304(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-308(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -144(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -72(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -112(%rbp)
	movq	-72(%rbp), %rax
	subq	-96(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -136(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-56(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -96(%rbp)
	movq	-112(%rbp), %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-144(%rbp), %rax
	movb	%dl, (%rax)
	movq	-144(%rbp), %rax
	leaq	3(%rax), %rdx
	movq	-112(%rbp), %rax
	subq	-72(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-136(%rbp), %rcx
	movq	-96(%rbp), %rax
	addq	%rcx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-144(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-136(%rbp), %rax
	subq	-96(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$16, -24(%rbp)
	addl	$1, -28(%rbp)
.L147:
	cmpl	$7, -28(%rbp)
	jle	.L148
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE31:
jpeg_idct_3x6:
.LFB32:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movl	%r8d, -228(%rbp)
	movq	-200(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-208(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L150
.L151:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	addq	$1024, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$64, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	subq	$-128, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -72(%rbp)
	movq	-48(%rbp), %rax
	subq	-64(%rbp), %rax
	subq	-64(%rbp), %rax
	sarq	$11, %rax
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-72(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-72(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -104(%rbp)
	movq	-8(%rbp), %rax
	addq	$80, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$160, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rax
	addq	%rdx, %rax
	imulq	$2998, %rax, %rax
	movq	%rax, -72(%rbp)
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-112(%rbp), %rax
	subq	-104(%rbp), %rax
	salq	$13, %rax
	movq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rax
	subq	-104(%rbp), %rax
	subq	-112(%rbp), %rax
	salq	$2, %rax
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$11, %rax
	movl	%eax, %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	60(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$12, %rax
	movq	-80(%rbp), %rdx
	movl	%edx, %ecx
	movq	-72(%rbp), %rdx
	addl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$48, %rax
	movq	-80(%rbp), %rdx
	movl	%edx, %ecx
	movq	-72(%rbp), %rdx
	subl	%edx, %ecx
	movl	%ecx, %edx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	addq	$24, %rax
	movq	-88(%rbp), %rcx
	movq	-56(%rbp), %rdx
	addq	%rcx, %rdx
	sarq	$11, %rdx
	movl	%edx, (%rax)
	movq	-24(%rbp), %rax
	leaq	36(%rax), %rdx
	movq	-88(%rbp), %rax
	subq	-56(%rbp), %rax
	sarq	$11, %rax
	movl	%eax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$4, -24(%rbp)
.L150:
	cmpl	$2, -28(%rbp)
	jle	.L151
	leaq	-192(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L152
.L153:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-224(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-228(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -120(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	cltq
	addq	$16400, %rax
	movq	%rax, -48(%rbp)
	salq	$13, -48(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	imulq	$5793, %rax, %rax
	movq	%rax, -88(%rbp)
	movq	-48(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-88(%rbp), %rax
	subq	-88(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-24(%rbp), %rax
	addq	$4, %rax
	movl	(%rax), %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	imulq	$10033, %rax, %rax
	movq	%rax, -48(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-120(%rbp), %rax
	movb	%dl, (%rax)
	movq	-120(%rbp), %rax
	leaq	2(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-120(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-56(%rbp), %rax
	sarq	$18, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$12, -24(%rbp)
	addl	$1, -28(%rbp)
.L152:
	cmpl	$5, -28(%rbp)
	jle	.L153
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32:
jpeg_idct_2x4:
.LFB33:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movl	%r8d, -212(%rbp)
	movq	-184(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -40(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-192(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L155
.L156:
	movq	-8(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -48(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$64, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rax
	addq	%rdx, %rax
	salq	$13, %rax
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	subq	-56(%rbp), %rax
	salq	$13, %rax
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -80(%rbp)
	movq	-8(%rbp), %rax
	addq	$48, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$96, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	cltq
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	imulq	$4433, %rax, %rax
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	imulq	$6270, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rbp)
	movq	-88(%rbp), %rax
	imulq	$-15137, %rax, %rdx
	movq	-96(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rax, %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	leaq	48(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	movq	%rax, (%rdx)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %rdx
	addq	%rcx, %rdx
	movq	%rdx, (%rax)
	movq	-24(%rbp), %rax
	leaq	32(%rax), %rdx
	movq	-72(%rbp), %rax
	subq	-56(%rbp), %rax
	movq	%rax, (%rdx)
	addl	$1, -28(%rbp)
	addq	$2, -8(%rbp)
	addq	$4, -16(%rbp)
	addq	$8, -24(%rbp)
.L155:
	cmpl	$1, -28(%rbp)
	jle	.L156
	leaq	-176(%rbp), %rax
	movq	%rax, -24(%rbp)
	movl	$0, -28(%rbp)
	jmp	.L157
.L158:
	movl	-28(%rbp), %eax
	cltq
	leaq	0(,%rax,8), %rdx
	movq	-208(%rbp), %rax
	addq	%rdx, %rax
	movq	(%rax), %rdx
	movl	-212(%rbp), %eax
	addq	%rdx, %rax
	movq	%rax, -104(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	addq	$33587200, %rax
	movq	%rax, -64(%rbp)
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	-64(%rbp), %rdx
	movq	-48(%rbp), %rax
	addq	%rdx, %rax
	sarq	$16, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %edx
	movq	-104(%rbp), %rax
	movb	%dl, (%rax)
	movq	-104(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-64(%rbp), %rax
	subq	-48(%rbp), %rax
	sarq	$16, %rax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-40(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	addq	$16, -24(%rbp)
	addl	$1, -28(%rbp)
.L157:
	cmpl	$3, -28(%rbp)
	jle	.L158
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33:
jpeg_idct_1x2:
.LFB34:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movl	%r8d, -68(%rbp)
	movq	-40(%rbp), %rax
	movq	440(%rax), %rax
	subq	$384, %rax
	movq	%rax, -8(%rbp)
	movq	-48(%rbp), %rax
	movq	88(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	-56(%rbp), %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -20(%rbp)
	addl	$4100, -20(%rbp)
	movq	-56(%rbp), %rax
	addq	$16, %rax
	movzwl	(%rax), %eax
	movswl	%ax, %edx
	movq	-16(%rbp), %rax
	addq	$32, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	movl	%eax, -24(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rdx
	movl	-68(%rbp), %eax
	addq	%rax, %rdx
	movl	-20(%rbp), %ecx
	movl	-24(%rbp), %eax
	addl	%ecx, %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-8(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	movq	-64(%rbp), %rax
	addq	$8, %rax
	movq	(%rax), %rdx
	movl	-68(%rbp), %eax
	addq	%rax, %rdx
	movl	-20(%rbp), %eax
	subl	-24(%rbp), %eax
	sarl	$3, %eax
	cltq
	andl	$1023, %eax
	movq	%rax, %rcx
	movq	-8(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rax), %eax
	movb	%al, (%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE34:
	.size	jpeg_idct_1x2, .-jpeg_idct_1x2
	.section	.rodata
	.type	__PRETTY_FUNCTION__.4216, @object
	.size	__PRETTY_FUNCTION__.4216, 5
__PRETTY_FUNCTION__.4216:
	.string	"cmov"
	.ident	"GCC: (Debian 4.9.2-10+deb8u2) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
